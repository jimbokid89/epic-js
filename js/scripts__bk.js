'use strict'

$(function() {
  $('.js-redact').on('click', function() {
    $('.redact').each(function() {
      $(this).next().val($(this).text());
    });
    $('.redact , .edit-subscriptions').hide();
    $('.redact-hide , .save-changes , .cancel-changes').show();
  });
  $('.js-redact-cancel').on('click', function() {
    $('.redact , .edit-subscriptions').show();
    $('.redact-hide , .save-changes , .cancel-changes').hide();
  });
  $('.js-redact-save').on('click', function() {
    // $('.redact-hide').each(function(){
    // 	$(this).text($(this).prev().val());
    // });
    $('.redact , .edit-subscriptions').show();
    $('.redact-hide , .save-changes , .cancel-changes').hide();
  });


  $('.js-redact-subs').on('click', function() {
    $('.redact-subs').each(function() {
      $(this).next().val($(this).text());
    });
    $('.redact-subs , .edit-subscriptions-subs , .hide-row-subs').hide();
    $('.redact-hide-subs , .save-changes-subs , .cancel-changes-subs , .add-item-subs a , .remove-goods-subs a , .show-row-subs').show();
  });
  $('.js-redact-cancel-subs').on('click', function() {
    $('.redact-subs , .edit-subscriptions-subs , .hide-row-subs').show();
    $('.redact-hide-subs , .save-changes-subs , .cancel-changes-subs, .add-item-subs a , .remove-goods-subs a , .show-row-subs').hide();
  });
  $('.js-redact-save-subs').on('click', function() {
    $('.redact-subs , .edit-subscriptions-subs , .hide-row-subs').show();
    $('.redact-hide-subs , .save-changes-subs , .cancel-changes-subs, .add-item-subs a , .remove-goods-subs a , .show-row-subs').hide();
  });


  //18.02.2016 Kovalchuk Yuriy

  var cartBtn = $('.js-add-cart');

  cartBtn.on('click', function(e) {
    e.preventDefault();

    var parentItem = this.closest('.product-item'),
      nameProd, sizeProd, imgProd, idProd, priceProd, discountProd;

    for (var i = 0; i <= localStorage.length; i++) {
      var taskID = "buy-" + (Math.floor((Math.random() * 10000) + 1));

      for (var key in localStorage) {
        if (key === taskID){
          taskID = "buy-" + (Math.floor((Math.random() * 10000) + 1));
        }
      }
    }

    idProd = $(parentItem).attr('data-id');
    nameProd = $(parentItem).find('.product-name').attr('data-name');
    sizeProd = $(parentItem).find('.product-size li.active').attr('data-size');
    imgProd = $(parentItem).find('.product-img img').attr('src');
    discountProd = $(parentItem).find('.product-subscribe input').is(":checked");
    priceProd = $(parentItem).find('.product-price > span').attr('data-price');

    if (discountProd) {
      priceProd = priceProd * 0.8;
    }

    var prod = {
      id: idProd,
      buy: taskID,
      name: nameProd,
      img: imgProd,
      size: sizeProd,
      price: priceProd,
      discount: discountProd,
      quantity: 1,
    }
    localStorage.setItem(taskID, JSON.stringify(prod));

  });
  basket();
});

$(window).on('load', function() {
  var link = decodeURIComponent(document.location.href);
  //Получаем строку ->> переводим в объектв
  link = link.split('#');
  //Проверка на наличии хеша в ссылке
  if (link[1]) {
    console.log(link[1]);
    localStorage.clear();
    link = link[1].replace('}{', '},{');
    link = link.substring(0, link.length - 1);
    link = link.split('},');

    var summ = link.length;
    //Теперь берем объекты и пихаем в localStorage
    for (var n = 0; n < summ; n++) {
      var tempObj = (link[n] + '}');
      tempObj = "'" + tempObj + "'";
      tempObj = JSON.parse(tempObj.slice(1, -1));
      localStorage.setItem(tempObj.buy, JSON.stringify(tempObj));
    };

    basket();

    var finalLink = document.location.href;
    finalLink = finalLink.split('#');
    window.location = finalLink[0];
  }
})

function basket() {

  var table = $('.basket-table');
  for (var n = 0, len = localStorage.length; n < len; ++n) {
    var user = JSON.parse(localStorage.getItem(localStorage.key(n)));
    if (user.id) {
      table.find('tbody').append('<tr data-attr="' + user.buy + '"><td><img src="' + user.img + '" alt=""></td><td>' + user.name + '</td><td>' + user.size + '</td><td><div class="spinner_wrapper"><div class="input-group spinner"><div class="input-group-btn-vertical"><button type="button">-</button><input type="text" value="' + user.quantity + '"><button type="button">+</button></div></div></div></td><td><div class="bask-sum" attr-price="' + user.price + '">' + (user.price * user.quantity) + '</div></td><td class="middle"><div class="bask-remove"><a href="#" class="js-remove-basket">rem</a></div></td></tr>')
    }
    if (n == (len - 1)) {
      bindingDelete();
    }
  }
  finalSumFn();
}

function finalSumFn() {
  var finalSum = 0;

  $('.basket-table').find('.bask-sum').each(function() {
    finalSum = finalSum + parseInt($(this).html());
  });

  $('.new-prise').html(finalSum);
}

function bindingDelete() {
  $('.js-remove-basket').on('click', function(e) {
    e.preventDefault();
    var removeAttr = $(this).closest('tr');
    $(removeAttr).remove();
    localStorage.removeItem(removeAttr.attr('data-attr'));

    finalSumFn();
  });

  $('.spinner button:last-of-type').on('click', function() {
    var num = $(this).siblings('input').val();
    var change;
    num++;

    $(this).siblings('input').val(num).attr('value', num);

    change = $(this).closest('tr').attr('data-attr');
    var user = JSON.parse(localStorage.getItem(change));
    user.quantity = num;
    localStorage.setItem(change, JSON.stringify(user));

    var finalPrice = $(this).closest('tr').find('.bask-sum');
    finalPrice.html((finalPrice.attr('attr-price') * num));

    finalSumFn();
  });

  $('.spinner button:first-of-type').on('click', function() {
    var num = $(this).siblings('input').val();
    var change;
    if (num == 1) {
      return;
    }
    num--;
    $(this).siblings('input').val(num).attr('value', num);

    change = $(this).closest('tr').attr('data-attr');
    var user = JSON.parse(localStorage.getItem(change));
    user.quantity = num;
    localStorage.setItem(change, JSON.stringify(user));

    var finalPrice = $(this).closest('tr').find('.bask-sum');
    finalPrice.html((finalPrice.attr('attr-price') * num));

    finalSumFn();
  });
}
//***********************************************************


$('.js-checkout').on('click', function(e) {
  e.preventDefault();
  var userStr = "";
  for (var n = 0, len = localStorage.length; n < len; ++n) {
    var user = JSON.parse(localStorage.getItem(localStorage.key(n)));

    //Проверка на точто наш обьект с localStorage является товаром. проверяемой по наличию свойства - ID
    if (user.id !== undefined) {
      userStr = userStr + JSON.stringify(user);
    }

  }
  window.location.hash = userStr;
  var link = decodeURIComponent(document.location.href);
});
